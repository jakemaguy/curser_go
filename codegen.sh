#!/usr/bin/env bash
export PATH="$PATH:$HOME/.local/bin:${HOME}/go/bin"

go get google.golang.org/grpc

protoc --go_out=. --go_opt=paths=source_relative \
    --go-grpc_out=. --go-grpc_opt=paths=source_relative \
    src/proto/kvmswitch.proto
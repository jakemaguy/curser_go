package main

import (
	"context"
	"flag"
	"io"
	"log"
	"time"

	pb "../proto"
	"google.golang.org/grpc"
)

var (
	serverAddr = flag.String("server_addr", "localhost:10000", "Usage:	<IP_ADDRESS>:<PORT_NUMBER>")
)

type kvmSwitchClient struct {
}

func SendMouseCoordinates(client pb.KVMSwitchClient) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	stream, err := client.SendMouseCoordinates(ctx)
	if err != nil {
		log.Fatalf("%v.RouteChat(_) = _, %v", client, err)
	}
	waitc := make(chan struct{})
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				close(waitc)
				return
			}
			if err != nil {
				log.Fatalf("Failed to receive a note : %v", err)
			}
			log.Printf("Got XCoordinate %d Got YCoordinate: %d", in.XCoordinate, in.YCoordinate)
		}
	}()
	if err := stream.Send(&pb.ErrorCode{Errorcode: 0}); err != nil {
		log.Fatalf("Failed to send a note: %v", err)
	}
	stream.CloseSend()
	<-waitc
}

func main() {
	flag.Parse()
	conn, err := grpc.Dial(*serverAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Failed to Dial: %v", err)
	}
	defer conn.Close()
	client := pb.NewKVMSwitchClient(conn)

	for {
		SendMouseCoordinates(client)
	}

}

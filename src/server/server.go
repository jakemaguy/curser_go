package main

import (
	"fmt"
	"io"
	"log"
	"net"

	"github.com/go-vgo/robotgo"
	"google.golang.org/grpc"

	pb "../proto"
)

type kvmSwitchServer struct {
	pb.UnimplementedKVMSwitchServer
	mouseCoordinates *pb.MouseCoordinates
	errorCode        *pb.ErrorCode
}

func serialize(mouseCoordinates *pb.MouseCoordinates) string {
	return fmt.Sprintf("%d %d", mouseCoordinates.XCoordinate, mouseCoordinates.YCoordinate)
}

func newServer() *kvmSwitchServer {
	s := &kvmSwitchServer{}
	return s
}

func getMouseCoordinates() *pb.MouseCoordinates {
	x, y := robotgo.GetMousePos()
	return &pb.MouseCoordinates{
		XCoordinate: uint32(x),
		YCoordinate: uint32(y),
	}
}

func (s *kvmSwitchServer) SendMouseCoordinates(stream pb.KVMSwitch_SendMouseCoordinatesServer) error {
	for {
		// send coordinates to client
		// TODO: Get current coordinates of screen
		currentCoordinates := getMouseCoordinates()
		if err := stream.Send(currentCoordinates); err != nil {
			return err
		}

		// Recieve Error message from client
		in, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		log.Printf("Received error code: %d from client", in.Errorcode)
	}
}

func main() {
	// X, err := xgb.NewConn()
	// if err != nil {
	// 	log.Fatalf("Failed to Create X connection: %v", err)
	// }
	// wid, _ := xproto.NewWindowId(X)
	// screen := xproto.Setup(X).DefaultScreen(X)
	// xproto.CreateWindow(X,
	// 	screen.RootDepth,
	// 	wid,
	// 	screen.Root,
	// 	1081, -1, // x and y
	// 	50, 50, // width, height
	// 	0, // border-width
	// 	xproto.WindowClassInputOutput,
	// 	screen.RootVisual,
	// 	xproto.CwEventMask|xproto.CwOverrideRedirect,
	// 	[]uint32{1, xproto.EventMaskPropertyChange}) // masks)
	// xproto.MapWindow(X, wid)

	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", 10000))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterKVMSwitchServer(s, newServer())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
